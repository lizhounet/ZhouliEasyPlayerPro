﻿using System.Windows.Forms;

namespace EasyPlayerProClient
{
    partial class PalyerMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SuspendLayout();
            // 
            // PalyerMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1279, 666);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.MaximizeBox = false;
            this.Name = "PalyerMain";
            this.Text = "PalyerMain";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PalyerMain_Load);
            this.FormClosing += new FormClosingEventHandler(this.PalyerMain_Closing);
            this.ResumeLayout(false);
        }

        #endregion
        #region 初始化界面
        private static System.Collections.Concurrent.ConcurrentDictionary<string, System.Windows.Forms.Panel> Palyers = new System.Collections.Concurrent.ConcurrentDictionary<string, System.Windows.Forms.Panel>();
        /// <summary>
        /// 初始化界面
        /// </summary>
        private void InitPlayerMain()
        {
            var PanelWidth = this.Width / 3;//每个Panel的宽度
            var PanelHeight = (this.Height - System.Windows.Forms.SystemInformation.MenuHeight - System.Windows.Forms.SystemInformation.CaptionHeight) / 3;//每个Panel的高度=父窗体高度-菜单栏高度-标题栏高度
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    int index = (i == 0 ? j + 1 : i == 1 ? 4 + j : 7 + j)-1;
                    int height = System.Convert.ToInt32(PanelHeight * 0.92);
                    System.Windows.Forms.Panel panel = new System.Windows.Forms.Panel();
                    panel.Size = new System.Drawing.Size(PanelWidth, PanelHeight);
                    panel.Name = "panel" + index;
                    panel.Location = new System.Drawing.Point(PanelWidth * j, PanelHeight * i);
                    panel.Paint += new System.Windows.Forms.PaintEventHandler(panel_Paint);
                    System.Windows.Forms.Panel paneldibu = new System.Windows.Forms.Panel();
                    paneldibu.Name = "paneldibu";
                    paneldibu.Width = PanelWidth;
                    paneldibu.Location = new System.Drawing.Point(1,System.Convert.ToInt32(PanelHeight * 0.92));
                    //paneldibu.BackColor = System.Drawing.Color.Red;
                    System.Windows.Forms.Label label = new System.Windows.Forms.Label();
                    label.Text = "Url:";
                    label.Width = 30;
                    label.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
                    label.Location=new System.Drawing.Point(10,0);
                    paneldibu.Controls.Add(label);
                    System.Windows.Forms.TextBox textBox = new System.Windows.Forms.TextBox();
                    textBox.Width = System.Convert.ToInt32(PanelWidth * 0.3);
                    textBox.Name = "textBox" + index;
                    textBox.Location = new System.Drawing.Point(label.Width + 10, 0);
                    paneldibu.Controls.Add(textBox);
                    CheckBox checkBox = new CheckBox();
                    checkBox.Text = "TCP";
                    checkBox.Name = "checkBoxTCP" + index;
                    checkBox.Checked = true;
                    checkBox.Width = System.Convert.ToInt32(PanelWidth * 0.1);
                    checkBox.Location = new System.Drawing.Point(label.Width + 20 + textBox.Width, 0);
                    paneldibu.Controls.Add(checkBox);
                    CheckBox checkBox1 = new CheckBox();
                    checkBox1.Text = "声音";
                    checkBox1.Checked = false;
                    checkBox1.Name = "checkBoxSY" + index;
                    checkBox1.Width = System.Convert.ToInt32(PanelWidth * 0.1);
                    checkBox1.Location = new System.Drawing.Point(label.Width + 20 + textBox.Width + checkBox.Width,0);
                    paneldibu.Controls.Add(checkBox1);
                    ComboBox comboBox = new ComboBox();
                    comboBox.Name = "comboBox" + index;
                    comboBox.Items.Add("YUY2");
                    comboBox.Items.Add("YV12");
                    comboBox.Items.Add("RGB565");
                    comboBox.Items.Add("GDI");
                    comboBox.SelectedItem = "GDI";
                    comboBox.Width = System.Convert.ToInt32(PanelWidth * 0.2);
                    comboBox.Location = new System.Drawing.Point(label.Width + 20 + textBox.Width + checkBox.Width + checkBox1.Width,0);
                    paneldibu.Controls.Add(comboBox);
                    Button button = new Button();
                    button.Text = "播放";
                    button.Name = "button" + index;
                    button.Click += new System.EventHandler(Paler_Bofang);
                    button.Location = new System.Drawing.Point(label.Width + 30 + textBox.Width + checkBox.Width + checkBox1.Width + comboBox.Width, 0);
                    paneldibu.Controls.Add(button);
                    panel.Controls.Add(paneldibu);
                    //把播放容器添加到窗体
                    this.Controls.Add(panel);
                    //把播放容器添加到Palyers进行管理
                    Palyers.TryAdd("Palyer" + index, panel);
                }
            }
        }
        #endregion
    }
}