﻿using EasyPlayerProSDK;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace EasyPlayerProClient
{
    public partial class PalyerMain : Form
    {
        #region 全局变量
        //视频流回调
        private static List<PlayerSDK.EasyPlayerProCallBack> RealProCallBack = new List<PlayerSDK.EasyPlayerProCallBack>(9);
        //显示格式
        private static RENDER_FORMAT renderFormat = RENDER_FORMAT.RENDER_FORMAT_RGB24_GDI;
        private static List<int> channelIDs = new List<int>(9) { 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        private static bool isInit = false;
        public static IntPtr palyerHandle = IntPtr.Zero;
        #endregion
        #region 防止双击标题栏、移动窗体
        /// <summary>
        /// 防止双击标题栏、移动窗体
        /// </summary>
        /// <param name="m"></param>
        protected override void WndProc(ref Message m)
        {
            //拦截双击标题栏、移动窗体的系统消息
            if (m.Msg != 0xA3 && m.Msg != 0x0003 && m.WParam != (IntPtr)0xF012)
            {
                base.WndProc(ref m);
            }
        }
        #endregion
        #region 画播放器边框
        /// <summary>
        /// 画播放器边框
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void panel_Paint(object sender, PaintEventArgs e)
        {
            //使用红色虚线绘制边框  
            Pen pen1 = new Pen(Color.Gray, 1);
            pen1.DashStyle = System.Drawing.Drawing2D.DashStyle.Solid;
            pen1.DashPattern = new float[] { 4f, 2f };
            e.Graphics.DrawRectangle(pen1, 0, 0, ((Panel)sender).Width - 1, ((Panel)sender).Height - 1);
        }
        #endregion
        public PalyerMain()
        {
            InitializeComponent();

        }
        /// <summary>
        /// 播放事件
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Paler_Bofang(object sender, EventArgs e)
        {
            Button button = sender as Button;
            var index = Convert.ToInt32(button.Name.Replace("button", ""));
            string Palyer = "Palyer" + index;
            Panel panel = Palyers[Palyer];
            var dibuControl = panel.Controls["paneldibu"].Controls;//底部的控件合集
            var textBox = ((TextBox)dibuControl["textBox" + index]);
            var checkBoxTCP = ((CheckBox)dibuControl["checkBoxTCP" + index]);
            var checkBoxSY = ((CheckBox)dibuControl["checkBoxSY" + index]);
            if (string.IsNullOrEmpty(textBox.Text))
            {
                MessageBox.Show("请输入URL");
                return;
            }
            if (button.Text == "播放")
            {

                var streamURI = textBox.Text;
                EASY_CHANNEL_SOURCE_TYPE_ENUM sourceType = GetSourceTypeByStreamURI(streamURI);
                bool overtcpType = checkBoxTCP.CheckState == CheckState.Checked;
                int queueSize = sourceType == EASY_CHANNEL_SOURCE_TYPE_ENUM.EASY_CHANNEL_SOURCE_TYPE_HLS ? 1024 * 1024 * 5 : 1024 * 1024 * 2;
                channelIDs[index] = PlayerSDK.LibEasyPlayerPro_OpenStream(palyerHandle, sourceType, streamURI, MEDIA_TYPE.MEDIA_TYPE_VIDEO | MEDIA_TYPE.MEDIA_TYPE_AUDIO | MEDIA_TYPE.MEDIA_TYPE_EVENT, RealProCallBack[index], IntPtr.Zero, overTcp: overtcpType, queueSize: (uint)queueSize);
                int ret = PlayerSDK.LibEasyPlayerPro_StartPlayStream(palyerHandle, channelIDs[index], panel.Handle, renderFormat);
                //PlayerSDK.LibEasyPlayerPro_SetPlayFrameCache(palyerHandle, channelID, 6);
                PlayerSDK.LibEasyPlayerPro_SetScaleDisplay(palyerHandle, channelIDs[index], true, Color.AntiqueWhite);
                if (ret == 0)
                {
                    if (checkBoxSY.Checked)
                        PlayerSDK.LibEasyPlayerPro_StartPlaySound(palyerHandle, channelIDs[index]);
                    button.Text = "停止";
                }
            }
            else
            {
                int ret = PlayerSDK.LibEasyPlayerPro_StopPlayStream(palyerHandle, channelIDs[index]);
                if (ret == 0)
                {
                    button.Text = "播放";
                    channelIDs[index] = -1;
                    panel.Refresh();
                }
            }
        }
        /// <summary>
        /// 视频流回调
        /// </summary>
        /// <param name="callbackType"></param>
        /// <param name="channelId"></param>
        /// <param name="userPtr"></param>
        /// <param name="mediaType"></param>
        /// <param name="pBuf"></param>
        /// <param name="_frameInfo"></param>
        /// <returns></returns>
        private int RealProSourceCallBack(EASY_CALLBACK_TYPE_ENUM callbackType, int channelId, IntPtr userPtr, MEDIA_TYPE mediaType, IntPtr pBuf, ref EASY_FRAME_INFO _frameInfo)
        {
            return 0;
        }
        #region 获取播放源的协议

        private EASY_CHANNEL_SOURCE_TYPE_ENUM GetSourceTypeByStreamURI(string streamUri)
        {
            string _streamUri = streamUri.ToLower();
            if (_streamUri.IndexOf("rtsp") == 0)
                return EASY_CHANNEL_SOURCE_TYPE_ENUM.EASY_CHANNEL_SOURCE_TYPE_RTSP;
            if (_streamUri.IndexOf("rtmp") == 0)
                return EASY_CHANNEL_SOURCE_TYPE_ENUM.EASY_CHANNEL_SOURCE_TYPE_RTMP;
            if (_streamUri.IndexOf("http") == 0)
                return EASY_CHANNEL_SOURCE_TYPE_ENUM.EASY_CHANNEL_SOURCE_TYPE_HLS;
            if (_streamUri.IndexOf("file") == 0)
                return EASY_CHANNEL_SOURCE_TYPE_ENUM.EASY_CHANNEL_SOURCE_TYPE_FILE;

            return EASY_CHANNEL_SOURCE_TYPE_ENUM.EASY_CHANNEL_SOURCE_TYPE_RTSP;
        }
        #endregion
        private void PalyerMain_Load(object sender, EventArgs e)
        {
            InitPlayerMain();
            if (PlayerSDK.LibEasyPlayerPro_Create(ref palyerHandle, 128) == 0)
                isInit = true;
            this.RightToLeft = RightToLeft.Inherit;
            for (int i = 0; i < 9; i++)
            {
                RealProCallBack.Add(RealProSourceCallBack);
            }
        }
        private void PalyerMain_Closing(object sender, FormClosingEventArgs e)
        {
            if (isInit)
                PlayerSDK.LibEasyPlayerPro_Release(ref palyerHandle);
        }
    }
}
